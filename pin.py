#!/usr/bin/python3

import sys
import datetime

def initGame(users):
  #with open('startTime.txt', 'w') as file:
    #file.write(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    #file.write('\n')
  with open('players.txt', 'w') as file:
    out = '\n'.join(users)
    out += '\n';
    file.write(out)
  with open('currentGame.txt', 'w') as file:
    file.write('')

def getUsers():
  with open('players.txt', 'r') as file :
    filedata = file.readlines()
    filedata = [x.strip() for x in filedata]
    return filedata

def getUserIndex(name):
  return getUsers().index(name)

def initHand():
  with open('currentHand.txt', 'w') as file:
    file.write('-1 -1\n') #user bid index and bid
    for u in getUsers():
      file.write('-1 -1\n') #meld value and trick value.

def setBidder(args):
  ui = getUsers().index(args[0])
  ub = args[1]
  if int(ub) == -1:
    ui = -1
  filedata = []
  with open('currentHand.txt', 'r') as oldfile:
    filedata = oldfile.readlines()
    filedata = filedata[1:] #skip first line
    filedata = [x.strip() for x in filedata]
  with open('currentHand.txt', 'w') as newfile:
    temp = [str(ui), str(ub)]
    l = ' '.join(temp)
    newfile.write(l)
    newfile.write('\n')
    r = '\n'.join(filedata)
    newfile.write(r)
    newfile.write('\n')

def setMeld(args):
  ui = getUsers().index(args[0])
  um = args[1]
  filedata = []
  with open('currentHand.txt', 'r') as oldfile:
    filedata = oldfile.readlines()
    filedata = [x.strip() for x in filedata]
  if filedata[0].find('-1') != -1:
    print ('\nERROR: set bid before setting meld')
    return
  front = filedata[:ui+1]
  back = filedata[ui+2:]
  oldLine = str.split(filedata[ui+1])
  oldMeld = oldLine[0]
  oldTrick = oldLine[1]
  temp = [str(um), str(oldTrick)]
  l = ' '.join(temp)
  with open('currentHand.txt', 'w') as newfile:
    t = '\n'.join(front) #front always exist. first line is bid
    newfile.write(t)
    newfile.write('\n')
    newfile.write(l)
    newfile.write('\n')
    if back: #when setting meld for last player we avoid the extra line.
      newfile.write('\n'.join(back))
      newfile.write('\n')

def setTrick(args):
  ui = getUsers().index(args[0])
  ut = args[1]
  filedata = []
  with open('currentHand.txt', 'r') as oldfile:
    filedata = oldfile.readlines()
    filedata = [x.strip() for x in filedata]
  if filedata[0].find('-1') != -1:
    print ('\nERROR: set bid before setting tricks')
    return
  front = filedata[:ui+1]
  back = filedata[ui+2:]
  oldLine = str.split(filedata[ui+1])
  oldMeld = oldLine[0]
  oldTrick = oldLine[1]
  temp = [str(oldMeld), str(ut)]
  l = ' '.join(temp)
  with open('currentHand.txt', 'w') as newfile:
    t = '\n'.join(front) #front always exist. first line is bid
    newfile.write(t)
    newfile.write('\n')
    newfile.write(l)
    newfile.write('\n')
    if back: #when setting trick for last player we avoid the extra line.
      newfile.write('\n'.join(back))
      newfile.write('\n')

def newHand():
  newline = ''
  trickTotal = 0
  with open('currentHand.txt', 'r') as cfile:
    filedata = cfile.readlines()
    filedata = [x.strip() for x in filedata]
    for line in filedata[1:]:
      if not line:
        continue
      split = str.split(line)
      trickTotal += int(split[1])
    newline = ' '.join(filedata)
  if not newline:
    return False
  if newline.find('-1') != -1:
    print ('\nERROR: missing values in current hand')
    return False
  if trickTotal != 25:
    print ('\nERROR: total trick count should equal 25')
    return False
  with open('currentGame.txt', 'a') as newfile:
    newfile.write(newline)
    newfile.write('\n')
  return True

def dumpCurrentHand():
  players = getUsers()
  print ('\nCurrent hand:')
  with open('currentHand.txt', 'r') as oldfile:
    filedata = oldfile.readlines()
    filedata = [x.strip() for x in filedata]
    firstLine = str.split(filedata[0])
    bidLine = 'Bid Not Set'
    bui = int(firstLine[0])
    if bui != -1:
      username = players[bui]
      userbid = firstLine[1]
      bidLine = username + ' bid '+ userbid
    print ("  ", str(bidLine))
    i = 1 #first line is the bid info
    for p in players:
      stats = str.split(filedata[i])
      #stats = [x.strip() for x in stats] not sure how to get rid of that space after equals.
      print ('  ', p, '  meld=', stats[0], '  trick=', stats[1])
      i = i + 1

def dumpCurrentGame():
  players = getUsers()
  scores = [0]*len(players)
  lastBidderIndex = -1
  print ('\nCurrent Game:')
  with open('currentGame.txt', 'r') as cfile:
    filedata = cfile.readlines()
    filedata = [x.strip() for x in filedata]
    handIndex = 1
    for line in filedata:
      print ('  Hand ', str(handIndex), ':')
      fields = str.split(line)
      assert (len(fields) == 8),"Wrong number of fields in current game!"
      lastBidderIndex = int(fields[0])
      playerIndex = 0
      for p in players:
        cm = fields[playerIndex * 2 + 2]
        ct = fields[playerIndex * 2 + 3]
        total = int(cm) + int(ct)
        bidString = ''
        if int(fields[0]) == playerIndex:
          bidString = '  bid=' + fields[1]
          if total >= int(fields[1]):
            bidString = bidString + ' MADE'
            scores[playerIndex] = scores[playerIndex] + total
          else:
            bidString = bidString + ' SET'
            scores[playerIndex] = scores[playerIndex] - int(fields[1])
        else:
          scores[playerIndex] = scores[playerIndex] + total
        print ('    ', p, '  meld=', cm, '  trick=', ct, '  hand total=', str(total), '  current score=', str(scores[playerIndex]), bidString)
        playerIndex = playerIndex + 1
      handIndex = handIndex + 1
  print ('  Totals:')
  playerIndex = 0
  for p in players:
    print ('    ', players[playerIndex], scores[playerIndex])
    playerIndex = playerIndex + 1
  #check for winner.
  if lastBidderIndex != -1 and scores[lastBidderIndex] >= 120:
    print ('  WINNER:', players[lastBidderIndex])
  else:
    winners = []
    winnersScore = -1
    playerIndex = 0
    for s in scores:
      if s >= 120:
        if s > winnersScore:
          winners = []
          winners.append(playerIndex)
          winnersScore = s
        elif s == winnersScore:
          winners.append(playerIndex)
      playerIndex = playerIndex + 1
    if len(winners) > 0:
      winnersString = '  WINNERS '
      for w in winners:
        winnersString += players[w] + ' '
      winnersString += '  score:' + str(winnersScore)
      print (winnersString)

def dumpOptions():
  print ('pin.py COMMAND ARGS')
  print ('')
  print ('COMMAND = init, ARGS = player names')
  print ('COMMAND = setbid or sb, ARGS = playername value')
  print ('COMMAND = setmeld or sm, ARGS = playername value')
  print ('COMMAND = settrick or st, ARGS = playername value')
  print ('COMMAND = currenthand or ch, no ARGS')
  print ('COMMAND = newhand or nh, no ARGS')
  print ('COMMAND = currentgame or cg, no ARGS')

def main(argv):
    #print 'Number of arguments:', len(sys.argv), 'arguments.'
    #print 'Argument List:', str(sys.argv)
    
    if (len(sys.argv) < 2):
      dumpOptions()
      sys.exit()
    
    command = sys.argv[1]
    args = sys.argv[2:]
    if (command == 'init' and len(args) > 0):
      initGame(args)
      initHand()
      dumpCurrentHand()
    if ((command == 'setbid' or command == 'sb') and len(args) == 2):
      setBidder(args)
      dumpCurrentHand()
    if ((command == 'setmeld' or command == 'sm') and len(args) == 2):
      setMeld(args)
      dumpCurrentHand()
    if ((command == 'settrick' or command == 'st') and len(args) == 2):
      setTrick(args)
      dumpCurrentHand()
    if ((command == 'currenthand' or command == 'ch') and len(args) == 0):
      dumpCurrentHand()
    if ((command == 'newhand' or command == 'nh') and len(args) == 0):
      if newHand():
        initHand()
        dumpCurrentGame()
      dumpCurrentHand()
    if ((command == 'currentgame' or command == 'cg') and len(args) == 0):
      dumpCurrentGame()

if __name__ == "__main__":
    main(sys.argv)
